package utfpr.ct.dainf.if62c.avaliacao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * Tarefa que exibe a hora atual na saída padrão.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class MensagemTask extends TimerTask {
        
    private final Date currentTime = new Date();
    private final SimpleDateFormat sdf = new SimpleDateFormat("'Hora:' HH:mm:ss");
    Timer timerEspera = new Timer("Timer Esperando");

    @Override
    public void run() {
        currentTime.setTime(System.currentTimeMillis());
        
        int Minutos = Integer.parseInt( (sdf.format(currentTime)).substring(9, 11) );
        
        if( Minutos%2 == 1 || Minutos==1){
            timerEspera = new Timer("Timer Esperando");
            timerEspera.scheduleAtFixedRate(new MensagemEsperandoTask(), 10000, 10000);
        }
        
        else
            timerEspera.cancel();
        
        System.out.println(sdf.format(currentTime));
    
    }
    
    public void killTimer(){
        timerEspera.cancel();
    }
}